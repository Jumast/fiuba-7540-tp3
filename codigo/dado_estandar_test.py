from unittest import TestCase
from dados import DadoEstandar

SUMA_DE_PROBABILIDADES = 1.0
CANTIDAD_DE_CARAS = 6

class TestDadoEstandar(TestCase):

    def test_probabilidades_son_equiprobables(self):

        dado = DadoEstandar(CANTIDAD_DE_CARAS)

        # Obtengo sus probabilidades.
        # Este metodo no se usa en el juego pero se agrego para poder correr la prueba.
        probabilidades = dado.obtener_probabilidades()
        probabilidad_cara_1 = probabilidades[0]
        for i in range(1, len(probabilidades)):
            probabilidad_cara_i = probabilidades[i]
            self.assertEqual(probabilidad_cara_i, probabilidad_cara_1)

    def test_probabilidades_no_se_deben_modificar(self):
        dado = DadoEstandar(6)
        probabilidades = dado.obtener_probabilidades()

        # Modifico el resultado devuelto.
        probabilidad_anterior = probabilidades[0]
        probabilidades[0] = probabilidad_anterior + 1

        # Verifico que no se haya modificado su probabilidad dentro del dado.
        self.assertEqual(probabilidad_anterior, dado.obtener_probabilidades()[0])

    def test_probabilidades_deben_sumar_1(self):
        dado = DadoEstandar(6)
        probabilidades = dado.obtener_probabilidades()

        # Sumo los elementos del iterable
        suma = 0
        for cara in probabilidades:
            suma += cara
        # Pido que la suma sea 1
        self.assertAlmostEquals(SUMA_DE_PROBABILIDADES, suma)




