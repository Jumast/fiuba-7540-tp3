from unittest import TestCase
from dados import DadoCreciente

SUMA_DE_PROBABILIDADES = 1.0
CANTIDAD_DE_CARAS = 6


class TestDadoCreciente(TestCase):

    def test_probabilidades_son_crecientes(self):

        dado = DadoCreciente(CANTIDAD_DE_CARAS)
        probabilidades = dado.obtener_probabilidades()

        for i in xrange(0, len(probabilidades) - 1):
            self.assertTrue(probabilidades[i] < probabilidades[i + 1])

    def test_probabilidades_no_se_deben_modificar(self):
        dado = DadoCreciente(CANTIDAD_DE_CARAS)
        probabilidades = dado.obtener_probabilidades()

        # Modifico el resultado devuelto.
        probabilidad_anterior = probabilidades[0]
        probabilidades[0] = probabilidad_anterior + 1

        # Verifico que no se haya modificado su probabilidad dentro del dado.
        self.assertEqual(probabilidad_anterior, dado.obtener_probabilidades()[0])

    def test_probabilidades_deben_sumar_1(self):
        dado = DadoCreciente(CANTIDAD_DE_CARAS)
        probabilidades = dado.obtener_probabilidades()

        # Sumo los elementos del iterable
        suma = 0
        for cara in probabilidades:
            suma += cara

        # Pido que la suma sea 1
        self.assertEqual(SUMA_DE_PROBABILIDADES, suma)


