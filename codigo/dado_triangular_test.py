from unittest import TestCase
from dados import DadoTriangular

SUMA_DE_PROBABILIDADES = 1.0
CANTIDAD_DE_CARAS = 6

class TestDadoTriangular(TestCase):

    def test_probabilidades_deben_ser_triangulares(self):
        dado = DadoTriangular(CANTIDAD_DE_CARAS)
        probabilidades = dado.obtener_probabilidades()

        cara_media = len(probabilidades)/2
        probabilidad_media = probabilidades[cara_media]
        cara_anterior_a_media = cara_media - 1
        cara_siguiente_a_media = cara_media + 1

        for i in range(0, cara_anterior_a_media):
            probabilidad_actual = probabilidades[i]
            self.assertTrue(probabilidad_actual < probabilidad_media)

        for i in range(cara_siguiente_a_media, cara_anterior_a_media):
            probabilidad_actual = probabilidades[i]
            self.assertTrue(probabilidad_actual > probabilidad_media)

    def test_probabilidades_no_se_deben_modificar(self):
        dado = DadoTriangular(CANTIDAD_DE_CARAS)
        probabilidades = dado.obtener_probabilidades()

        # Modifico el resultado devuelto.
        probabilidad_anterior = probabilidades[0]
        probabilidades[0] = probabilidad_anterior + 1

        # Verifico que no se haya modificado su probabilidad dentro del dado.
        self.assertEqual(probabilidad_anterior, dado.obtener_probabilidades()[0])

    def test_probabilidades_deben_sumar_1(self):
        dado = DadoTriangular(CANTIDAD_DE_CARAS)
        probabilidades = dado.obtener_probabilidades()

        # Sumo los elementos del iterable
        suma = 0
        for cara in probabilidades:
            suma += cara

        # Pido que la suma sea 1
        self.assertAlmostEqual(SUMA_DE_PROBABILIDADES, suma)


