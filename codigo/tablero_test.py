import unittest
from unittest import TestCase
from tablero import Tablero


class TestTablero(TestCase):

    def test_inicializacion_conDistintosLargos_invalida(self):

        # arreglar
        casilleros = ['1', None, '3']
        posiciones = ['1', '2']

        # actuar - afirmar
        with self.assertRaises(ValueError):
            Tablero(casilleros, posiciones)

    def test_siguienteSentidoHorario_devuelveValorCorrecto(self):

        # arreglar
        casilleros = ["", "", "", "", "", "", ""]
        posiciones = [(), (), (), (), (), (), ()]  # cinco posiciones
        tablero = Tablero(casilleros, posiciones)
        PRIMERA_POS = 0
        ULTIMA_POS = 6  # 4 = len(posiciones)

        # actuar - afirmar
        self.assertEqual(tablero.siguiente_sentido_horario(ULTIMA_POS, 1), 5)
        self.assertEqual(tablero.siguiente_sentido_horario(5, 1), 4)
        self.assertEqual(tablero.siguiente_sentido_horario(PRIMERA_POS, 1), ULTIMA_POS)
        self.assertEqual(tablero.siguiente_sentido_horario(4, 3), 1)
        self.assertEqual(tablero.siguiente_sentido_horario(2, 2), 0)
        self.assertEqual(tablero.siguiente_sentido_horario(2, 3), 6)
        self.assertEqual(tablero.siguiente_sentido_horario(PRIMERA_POS, ULTIMA_POS + 1), PRIMERA_POS)
        self.assertEqual(tablero.siguiente_sentido_horario(4, 9), 2)
        self.assertEqual(tablero.siguiente_sentido_horario(3, 15), 2)
        self.assertEqual(tablero.siguiente_sentido_horario(5, 25), 1)
        self.assertEqual(tablero.siguiente_sentido_horario(0, 0), 0)

    def test_siguienteSentidoAntihorario_devuelveValorCorrecto(self):

        # arreglar
        casilleros = ["", "", "", "", "", "", ""]
        posiciones = [(), (), (), (), (), (), ()]  # cinco posiciones
        tablero = Tablero(casilleros, posiciones)
        PRIMERA_POS = 0
        ULTIMA_POS = 6  # 4 = len(posiciones)

        # actuar - afirmar
        self.assertEqual(tablero.siguiente_sentido_antihorario(ULTIMA_POS, 1), PRIMERA_POS)
        self.assertEqual(tablero.siguiente_sentido_antihorario(0, 1), 1)
        self.assertEqual(tablero.siguiente_sentido_antihorario(2, 3), 5)
        self.assertEqual(tablero.siguiente_sentido_antihorario(4, 3), 0)
        self.assertEqual(tablero.siguiente_sentido_antihorario(5, 2), 0)
        self.assertEqual(tablero.siguiente_sentido_antihorario(PRIMERA_POS, ULTIMA_POS + 1), PRIMERA_POS)
        self.assertEqual(tablero.siguiente_sentido_antihorario(4, 9), 6)
        self.assertEqual(tablero.siguiente_sentido_antihorario(3, 15), 4)
        self.assertEqual(tablero.siguiente_sentido_antihorario(5, 25), 2)

    def test_casilleros_noSeDebenModificar(self):

        # arreglar
        tablero, casilleros, posiciones = self._crear_instancia()

        # actuar
        casillero_0 = tablero[0]
        casilleros[0] = "modificado"

        # afirmar
        self.assertEqual(casillero_0, tablero[0])

    def test_contenidoCasillero_posicionNegativa_invalida(self):

        # arreglar
        tablero, casilleros, posiciones = self._crear_instancia()

        # actuar - afirmar
        with self.assertRaises(IndexError):
            tablero[-2]

    def test_casillero_contenido_devuelveValorCorrecto(self):

        # arreglar
        tablero, casilleros, posiciones = self._crear_instancia()

        # actuar - afirmar

        # para cada posicion del tablero
        for indice, contenido in enumerate(casilleros):
            # verifico que el contenido del casillero es el mismo que se paso como parametro
            self.assertEqual(contenido, tablero[indice])

    def test_posiciones_noSeDebenModificar(self):

        # arreglar:
        tablero, _, posiciones = self._crear_instancia()

        # actuar
        posicion_0 = posiciones[0]
        posiciones[0] = (0, 0)

        # afirmar
        self.assertEqual(posicion_0, tablero.posicion_de_casillero(0))

    def test_posicionDelCasillero_posicionNegativa_invalida(self):

        # arreglar
        tablero, _1, _2 = self._crear_instancia()

        # actuar - afirmar
        with self.assertRaises(IndexError):
            tablero.posicion_de_casillero(-2)

    def test_posicionDelCasillero_devuelveValorCorrecto(self):

        # arreglar
        tablero, _, posiciones = self._crear_instancia()

        # actuar - afirmar

        # para cada posicion del tablero
        for indice, posicion in enumerate(posiciones):
            # verifico que la posicion del casillero sea la misma que se paso como parametro
            self.assertEqual(posicion, tablero.posicion_de_casillero(indice))

    def test_len_devuelveLaCantidadDeCasilleros(self):

        # arreglar
        tablero, casilleros, _ = self._crear_instancia()

        # actuar
        cantidad_de_casilleros = len(tablero)

        # afirmar
        self.assertEqual(cantidad_de_casilleros, len(casilleros))

    def _crear_instancia(self):

        casilleros = ["libre", "Terraza", "Andamio", "Aula 400", "Salon del CD", "E9"]
        posiciones = [
            (550, 255),
            (545, 580),
            (540, 300),
            (550, 175),
            (560, 200),
            (555, 225)
        ]
        tablero = Tablero(casilleros, posiciones)
        return tablero, casilleros, posiciones







