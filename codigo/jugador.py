#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Jugador(object):
    """Representa a un jugador manejado por un usuario.
    Todo el manejo para pedirle y mostrarle cosas al usuario se hace utilizando su atributo "pedidos"
    que se encarga de dichas tareas.
    En este modulo no puede haber ninguna funcion raw_input ni print, ni ninguna semejante."""

    def __init__(self, nombre, posicion_inicial, listado_inicial, dados, pedidos):
        """Recibe su nombre, una posicion inicial, un listado ya inicializado, los dados a usar
        y alguien que le permita hacerle pedidos al usuario, de la manera que corresponda."""

        self._nombre = nombre
        self._posicion_actual = posicion_inicial
        self._cartas_no_vistas = listado_inicial
        self._dados = dados
        self._pedidos = pedidos

        self._mano_inicial = []

    def get_nombre(self):
        """Devuelve el nombre del jugador"""

        return self._nombre

    def __eq___(self, otro):
        """Verifica si un jugador es igual a otro jugador.
        Dos jugadores son iguales cuando tienen el mismo nombre"""

        return self._nombre == otro._nombre

    def asignar_carta(self, carta):
        """Se le asigna una carta a la mano del jugador. Este la marca como vista en su listado
        de cartas."""

        self._mano_inicial.append(carta)
        self._cartas_no_vistas.sacar_carta(carta)

    def get_posicion(self):
        """Obtiene la posicion del jugador."""

        return self._posicion_actual

    def alguna_carta(self, jugada):
        """Se fija si el jugador tiene alguna de las cartas indicadas en la jugada.
        Parametros:
            - jugada: iterable con cartas.
        Salida: si tiene al menos una de las cartas, debe preguntarle al usuario cual
        prefiere mostrarle. Si no tiene ninguna, devuelve None."""

        posibles_cartas_a_mostrar = []
        for carta in jugada:
            if carta in self._mano_inicial:
                posibles_cartas_a_mostrar.append(carta)

        if len(posibles_cartas_a_mostrar) == 0:
            return None
        elif len(posibles_cartas_a_mostrar) == 1:
            return posibles_cartas_a_mostrar[0]
        else:
            carta_a_mostrar = self._pedidos.pedir_carta_a_mostrar(self, posibles_cartas_a_mostrar)
            return carta_a_mostrar

    def arriesgar(self):
        """Devuelve arriesgo del usuario (personaje, arma, jugador), o None si no desea arriesgarse."""

        return self._pedidos.preguntar_arriesgo()

    def mover(self, tablero):
        """Lanza los dados y se mueve en algun sentido por el tablero. Le muestra al usuario el resultado de
        haber lanzado los dados, y le pide el sentido en el que debe moverse."""

        sentido = self._pedidos.pedir_sentido()

        resultados = []
        movimiento = 0
        for dado in self._dados:
            resultado_lanzamiento_dado = dado.lanzar()

            resultados.append(resultado_lanzamiento_dado)
            movimiento += resultado_lanzamiento_dado

        posicion_actual = self._posicion_actual
        posicion_final = tablero.siguiente(posicion_actual, movimiento, sentido)
        self._posicion_actual = posicion_final

        self._pedidos.mostrar_dados(resultados)

    def sugerir(self, tablero, otros_jugadores):
        """Si esta en algun lugar para hacer sugerencias, le pregunta al usuario si desea hacer una.
        En caso afirmativo, le muestra la mano al jugador, le muestra el listado de cartas que aun no vio,
        le pide la jugada, y le consulta al resto de los jugadores si tiene alguna
        de las cartas.
        Parametros:
            - tablero: tablero del juego.
            - otros_jugadores: un iterable con los demas jugadores, en el orden en el que se les debe consultar."""

        contenido_casillero = tablero[self._posicion_actual]
        if contenido_casillero:
            quiere_hacer_sugerencia = self._pedidos.quiere_consultar(contenido_casillero)
            if quiere_hacer_sugerencia:
                self._pedidos.mostrar_mano(self._mano_inicial)
                self._pedidos.mostrar_listado(self._cartas_no_vistas)

                jugada_personaje = self._pedidos.pedir_personaje()
                jugada_arma = self._pedidos.pedir_arma()
                jugada_lugar = contenido_casillero
                jugada = [jugada_personaje, jugada_lugar, jugada_arma]

                for jugador in otros_jugadores:
                    carta_a_mostrar = jugador.alguna_carta(jugada)
                    if carta_a_mostrar:
                        self._pedidos.mostrar_carta(jugador, carta_a_mostrar)
                        self._cartas_no_vistas.sacar_carta(carta_a_mostrar)
                        return
                self._pedidos.mostrar_no_hay_cartas()


