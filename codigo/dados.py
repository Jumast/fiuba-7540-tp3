#!/usr/bin/env python
# -*- coding: utf-8 -*-

TIPO_DADO_ESTANDAR = "Estandar (todas las caras equiprobables)"
TIPO_DADO_CRECIENTE = "Creciente"
TIPO_DADO_DECRECIENTE = "Decreciente"
TIPO_DADO_TRIANGULAR = "Triangular"
TIPOS_DADOS = [TIPO_DADO_ESTANDAR, TIPO_DADO_CRECIENTE, TIPO_DADO_DECRECIENTE, TIPO_DADO_TRIANGULAR]

import random

class Dado(object):
    """Clase que representa un dado cargado para obtener un número
    aleatorio, con una probabilidad diferente por cada cara."""
    def __init__(self, prob_caras):
        """Recibe un iterable con las probabilidades de cada resultado
        del dado.

    Parametros:
      - prob_caras: un iterable con tantos elementos como caras
        tiene el dado. El elemento n-ésimo de este iterable es
        la probabilidad de la cara N.

        La suma de las probabilidades debe ser 1 (o muy similar),
        si no lanzará una excepcion de tipo ValueError."""
        self._prob_caras = prob_caras[:]

    def obtener_probabilidades(self):
        """Devuelve las probabilidades de las caras del dado.

        Devuelve:
        - un iterable cuya suma debe ser 1."""
        return self._prob_caras[:]

    def lanzar(self):
        """Lanza el dado y devuelve el resultado.

        Devuelve:
        - un entero entre 1 y N, siendo N el número de caras."""

        tirada = random.random()
        suma = 0
        for i in xrange(0, len(self._prob_caras)):
            suma += self._prob_caras[i]
            if tirada < suma:
                return i + 1

class DadoEstandar(Dado):
    """Clase que representa un dado con una distribucion de probabilidades
    estandar."""

    def __init__(self, caras):
        self.tipo_dado = "Estandar"
        lista_probabilidades = []
        nro_caras = float(caras)
        for i in xrange(caras):
            lista_probabilidades.append(1 / nro_caras)
        self._prob_caras = lista_probabilidades

class DadoCreciente(Dado):
    """Clase que representa un dado con una distribucion de probabilidades creciente."""

    def __init__(self, nro_caras):
        lista_probabilidades = []
        caras = float(nro_caras)
        for i in xrange(nro_caras):
            lista_probabilidades.append(1 / caras)
        for i in xrange(nro_caras - 1):   # carga el dado crecientemente
            lista_probabilidades[i] = lista_probabilidades[i] / 2
            lista_probabilidades[i + 1] += lista_probabilidades[i]
        self._prob_caras = lista_probabilidades

class DadoDecreciente(Dado):
    """Clase que representa un dado con una distribucion de probabilidades decreciente."""

    def __init__(self, nro_caras):
        lista_probabilidades = []
        caras = float(nro_caras)
        for i in xrange(nro_caras):
            lista_probabilidades.append(1/caras)
        for i in xrange(nro_caras - 1): # carga el dado crecientemente
            lista_probabilidades[i] = lista_probabilidades[i]/2
            lista_probabilidades[i + 1] += lista_probabilidades[i]
        self._prob_caras = lista_probabilidades[::-1]
    

class DadoTriangular(Dado):
    """Clase que representa un dado con una distribucion de probabilidades triangular:
    las caras cercanas al valor medio tienen mayor probabilidad, y a medida que nos alejamos
    de dicho valor la probabilidad va disminuyendo."""

    def __init__(self, caras):
        nro_caras = float(caras)
        lista_probabilidades = []
        for i in xrange(caras):    # crea un donde todas las caras tienen 1/(nro de caras) de probabilidad
            lista_probabilidades.append(1/nro_caras)
        if caras%2 == 0:    # dado de cantidad par de caras
            valor_medio = caras / 2
            for i in xrange(valor_medio):
                lista_probabilidades[i] -= (lista_probabilidades[i]) / 2
                lista_probabilidades[i + 1] += lista_probabilidades[i]
            for i in xrange(valor_medio - 1, caras - 1):
                lista_probabilidades[i] += (lista_probabilidades[i + 1]) / 2
                lista_probabilidades[i + 1] -= (lista_probabilidades[i + 1]) / 2
            self._prob_caras = lista_probabilidades
        else:   # dado de cantidad impar de caras
            if caras == 3:
                self._prob_caras = [float(1/4), float(1/2), float(1/4)]
            else:
                valor_medio = (caras/2) + 1
                for i in xrange(valor_medio):
                    lista_probabilidades[i] -= (lista_probabilidades[i]) / 2
                    lista_probabilidades[i + 1] += lista_probabilidades[i]
                for i in xrange (valor_medio, caras - 1):
                    lista_probabilidades[i] += (lista_probabilidades[i + 1]) / 2
                    lista_probabilidades[i + 1] -= (lista_probabilidades[i + 1]) / 2
                self._prob_caras = lista_probabilidades


GENERADORES = [DadoEstandar, DadoCreciente, DadoDecreciente, DadoTriangular]

