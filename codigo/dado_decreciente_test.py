from unittest import TestCase
from dados import DadoDecreciente

SUMA_DE_PROBABILIDADES = 1.0
CANTIDAD_DE_CARAS = 6

class TestDadoDecreciente(TestCase):

    def test_probabilidades_son_decrecientes(self):

        dado = DadoDecreciente(CANTIDAD_DE_CARAS)
        probabilidades = dado.obtener_probabilidades()

        # Comparo
        for i in xrange(0, len(probabilidades) - 1):
            probabilidad_actual = probabilidades[i]
            probabilidad_siguiente = probabilidades[i+1]
            self.assertTrue(probabilidad_actual > probabilidad_siguiente)

    def test_probabilidades_no_se_deben_modificar(self):
        dado = DadoDecreciente(CANTIDAD_DE_CARAS)
        probabilidades = dado.obtener_probabilidades()

        # Modifico el resultado devuelto.
        probabilidad_anterior = probabilidades[0]
        probabilidades[0] = probabilidad_anterior + 1

        # Verifico que no se haya modificado su probabilidad dentro del dado.
        self.assertEqual(probabilidad_anterior, dado.obtener_probabilidades()[0])

    def test_probabilidades_deben_sumar_1(self):
        dado = DadoDecreciente(CANTIDAD_DE_CARAS)
        probabilidades = dado.obtener_probabilidades()

        # Sumo los elementos del iterable
        suma = 0
        for cara in probabilidades:
            suma += cara

        # Pido que la suma sea 1
        self.assertAlmostEqual(SUMA_DE_PROBABILIDADES, suma)

