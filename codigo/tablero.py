#!/usr/bin/env python
# -*- coding: utf-8 -*-

HORARIO = 0
ANTIHORARIO = 1

SENTIDO_HORARIO = "Horario"
SENTIDO_ANTIHORARIO = "Antihorario"
SENTIDOS = [SENTIDO_HORARIO, SENTIDO_ANTIHORARIO]


class Tablero(object):
    """Representa un tablero circular de un juego con casilleros, en los cuales
    pueden haber espacios vacios, o con lugares."""

    def __init__(self, casilleros, posiciones):
        """Recibe una lista de casilleros. Cada casillero debe contener una cadena
        con el contenido del casillero, en la posicion indicada, o None si no hay nada."""

        if len(casilleros) != len(posiciones):
            raise ValueError("Los casilleros y las posiciones no tienen la misma cantidad de elementos.")

        self._casilleros = casilleros[:]
        self._posiciones = posiciones[:]

        self._PRIMERA_POSICION = 0
        self._ULTIMA_POSICION = len(casilleros) - 1

    def siguiente_sentido_horario(self, pos, movimiento):
        """Devuelve la siguiente posicion en sentido horario.
        Parametros:
            - pos: numero de posicion actual
            - movimiento: cantidad de casilleros a desplazarse.
        Salida: nueva posicion, resultante de moverse en sentido horario
        una cantidad "moviemiento" de casilleros"""

        pos_final = pos
        rango = range(1, movimiento + 1)
        for n in rango:
            if pos_final == self._PRIMERA_POSICION:
                pos_final = self._ULTIMA_POSICION
                continue
            pos_final -= 1
        return pos_final

    def siguiente_sentido_antihorario(self, pos, movimiento):
        """Devuelve la siguiente posicion en sentido antihorario.
        Parametros:
            - pos: numero de posicion actual
            - movimiento: cantidad de casilleros a desplazarse.
        Salida: nueva posicion, resultante de moverse en sentido antihorario
        una cantidad "moviemiento" de casilleros"""

        pos_final = pos
        rango = range(1, movimiento + 1)
        for n in rango:
            if pos_final == self._ULTIMA_POSICION:
                pos_final = self._PRIMERA_POSICION
                continue
            pos_final += 1
        return pos_final

    def siguiente(self, pos, movimiento, sentido):
        """Devuelve la posicion siguiente en el sentido indicado.
        Parametro:
            - pos: numero de posicion actual.
            - moviemiento: cantidad de casilleros a desplazarse.
            - sentido: HORARIO o ANTIHORARIO, para indicar el sentido deseado.
        Salida: nueva posicion, resultante de moverse en el sentido indicado,
        una cantidad "moviemiento" de casilleros"""
        if sentido not in [HORARIO, ANTIHORARIO]:
            raise ValueError("Sentido incorrecto.")

        if sentido == HORARIO:
            return self.siguiente_sentido_horario(pos, movimiento)
        elif sentido == ANTIHORARIO:
            return self.siguiente_sentido_antihorario(pos, movimiento)

    def __getitem__(self, pos):
        """Obtiene el contenido del casillero indicado.
        Parametros:
            - pos: numero de casillero del cual se quiere obtener.
        Salida: el lugar que representa a tal casillero, o None si no hay nada."""
        if pos < 0:
            raise IndexError("La posicion no puede ser negativa.")
        return self._casilleros[pos]

    def __len__(self):
        """Obtiene la cantidad de casilleros del tablero."""
        return len(self._casilleros)

    def posicion_de_casillero(self, pos):
        """Devuelve una tupla (posicion X, posicion Y) con las posiciones en el mapa
        del casillero recibido por parametro.
        Parametros:
            - pos: numero de casillero del cual se quiere obtener.
        """
        if pos < 0:
            raise IndexError("La posicion no puede ser negativa")
        return self._posiciones[pos]
