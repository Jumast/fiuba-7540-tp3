#!/usr/bin/env python
# -*- coding: utf-8 -*-

NEW_LINE = "\n"

class ListadoCartas(object):
    """Representa el listado de cartas que un jugador aun no visualizo. Permite llevar cuenta de las cartas
    que ya se vieron, para saber cuales conviene consultar."""

    def __init__(self, personajes_inicial, armas_inicial, lugares_inicial):
        """Recibe un iterable para las cartas de _personajes_no_vistos, _armas_no_vistas y _lugares_no_vistos."""
        self._personajes_no_vistos = personajes_inicial[:]
        self._armas_no_vistas = armas_inicial[:]
        self._lugares_no_vistos = lugares_inicial[:]

    def __str__(self):
        """Convierte el listado en una cadena"""

        cadena = ''

        tipo_de_carta = "Personajes"
        cadena += tipo_de_carta + ": " + NEW_LINE + "".join(["=" for c in tipo_de_carta]) + NEW_LINE
        for elemento in self._personajes_no_vistos:
            cadena += elemento + NEW_LINE
        cadena += NEW_LINE

        tipo_de_carta = "Armas"
        cadena += tipo_de_carta + ": " + NEW_LINE + "".join(["=" for c in tipo_de_carta]) + NEW_LINE
        for elemento in self._armas_no_vistas:
            cadena += elemento + NEW_LINE
        cadena += NEW_LINE

        tipo_de_carta = "Lugares"
        cadena += tipo_de_carta + ": " + NEW_LINE + "".join(["=" for c in tipo_de_carta]) + NEW_LINE
        for elemento in self._lugares_no_vistos:
            cadena += elemento + NEW_LINE
        cadena += NEW_LINE

        # Elimino el ultimo ", "
        cadena = cadena[:-2]

        return cadena

    def sacar_carta(self, carta):
        """Saca una determinada carta de los listados de personajes, armas y lugares (los marca como "vistos")"""

        if carta in self._personajes_no_vistos:
            self._personajes_no_vistos.remove(carta)
        elif carta in self._armas_no_vistas:
            self._armas_no_vistas.remove(carta)
        elif carta in self._lugares_no_vistos:
            self._lugares_no_vistos.remove(carta)
        else:
            raise ValueError
